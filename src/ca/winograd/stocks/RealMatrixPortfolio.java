package ca.winograd.stocks;

import java.util.List;

import org.apache.commons.math3.linear.RealMatrix;

class RealMatrixPortfolio implements Portfolio {

	private final BiMap mKeyMapping;
	private final RealMatrix mWeights;

	public RealMatrixPortfolio(List<String> stocks, RealMatrix weights) {
		this(new BiMap(stocks), weights);
	}

	public RealMatrixPortfolio(BiMap mapping, RealMatrix weights) {
		mWeights = weights.copy();
		mKeyMapping = mapping;
	}

	public RealMatrix getRealMatrix() {
		return mWeights.copy();
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		for(int i = 0; i < mKeyMapping.size(); i++) {
			builder.append(String.format("%s : %.2f%%", mKeyMapping.getValue(i), 100 * mWeights.getEntry(i, 0))).append("\n");
		}
		
		return builder.toString();
	}

}
