package ca.winograd.stocks;

public class Preconditions {

	public static void checkArgument(boolean arg, String val) {
		if(arg != true) {
			throw new IllegalArgumentException(val);
		}
	}
	
}
