package ca.winograd.stocks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.LogConfigurationException;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.LogSource;
import org.apache.commons.logging.impl.Log4JLogger;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.stat.correlation.Covariance;

import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;

public class StockService {

	public static void main(String [] args) throws IOException {
		// Turn off stupid logging
		System.setProperty("org.apache.commons.logging.Log",
                "org.apache.commons.logging.impl.NoOpLog");
		
		// Take some Tickers
		final String [] symbols = new String[] {"SPY", "TLT", "RWO", "JNK", "MDY", "AMZN", "TSLA", "SCHF", "SCHE"};
		BiMap keyMappings = new BiMap(symbols);
		
		Map<String, Stock> stocks = YahooFinance.get(symbols);

		final Calendar start = Calendar.getInstance();
		start.add(Calendar.YEAR, -3);
		
		final Calendar end = Calendar.getInstance();

		Map<String, List<Float>> returns = findHistorialReturns(stocks, start, end, Interval.DAILY);
		Covariance covariance = new Covariance(flattenReturns(keyMappings, returns));
		RealMatrix covarianceMatrix = covariance.getCovarianceMatrix().scalarMultiply(250);
		Map<String, Double> risks = findRisks(keyMappings, covarianceMatrix);
		
		// TODO: Numerical model that doesn't allow short sales
		Map<String, Float> expectedReturns = findExpectedReturns(returns);
		//final EfficientFrontier riskCalculator = LagrangeEfficientFrontier.newInstance(keyMappings, toMatrix(expectedReturns), covarianceMatrix);
		final EfficientFrontier riskCalculator = QPEfficientFrontier.newInstance(keyMappings, toMatrix(expectedReturns), covarianceMatrix);
		
		// Equation
		System.out.println("Efficient Frontier w/ short selling: sigma^2 = " + riskCalculator);
		
		// How does pure stock investment compare?
		System.out.println("\nTicker\tReturn\tExpct\tActual");
		for(Entry<String, Float> entry : expectedReturns.entrySet()) {
			double u = entry.getValue(); 
			double expected = riskCalculator.getRisk(u);

			double actual = risks.get(entry.getKey());
			System.out.println(String.format("%s\t%.3f%%\t%.3f%%\t%.3f%%", entry.getKey(), entry.getValue(), expected, actual));
		}
		
		// How can we do overall?
		System.out.println("\nTable");
		for(int u = 0; u < 30; u++) {
			double risk = riskCalculator.getRisk(u);
			System.out.println(String.format("%d.0%%\t%.3f%%", u, risk));
		}
		
		// Portfolios:
		System.out.println("\nGlobal Minimum:");
		System.out.println(riskCalculator.getGlobalMinimumPortfolio());

		System.out.println("\nTangency Portfolio:");
		System.out.println(riskCalculator.getTangencyPortfolio());

		float min = min(expectedReturns.values()) + 0.01f;
		float max = max(expectedReturns.values());
		
		for(int u = (int) Math.ceil(min); u <= max; u+=5) {
			System.out.println("\nPortfolio(u = " + u + "%):");
			System.out.println(riskCalculator.getPortfolio(u));
		}
		
		
		XYPlotMapper plotter = new XYPlotMapper();
		plotter.plot(min, max, 0.1, new XYPlotMapper.Function() {
			
			@Override
			public double getValue(double input) {
				return riskCalculator.getRisk(input);
			}
		});
	}

	private static <T extends Comparable<T>> T min(Iterable<T> vals) {
		T min = null;
		for(T val : vals) {
			if(min == null || min.compareTo(val) > 0) {
				min = val;
			}
		}
		
		return min;
	}
	private static <T extends Comparable<T>> T max(Iterable<T> vals) {
		T min = null;
		for(T val : vals) {
			if(min == null || min.compareTo(val) < 0) {
				min = val;
			}
		}
		
		return min;
	}

	private static RealMatrix toMatrix(Map<String, Float> expectedReturns) {
		double [] eReturns = new double[expectedReturns.size()];
		int i = 0;
		for(Map.Entry<String, Float> entry : expectedReturns.entrySet()) {
			eReturns[i] = entry.getValue().floatValue();
			i++;
		}
		RealMatrix returnsMatrix = MatrixUtils.createColumnRealMatrix(eReturns);
		return returnsMatrix;
	}

	private static Map<String, Double> findRisks(BiMap mappings, RealMatrix covarianceMatrix) {
		Map<String, Double> risks = new HashMap<String, Double>();
		for(int i = 0; i < covarianceMatrix.getColumnDimension(); i++) {
			double standardDev = Math.sqrt(covarianceMatrix.getEntry(i, i));
			risks.put(mappings.getValue(i), standardDev);
		}
		return risks;
	}

	private static double[][] flattenReturns(BiMap mappings, Map<String, List<Float>> returns) {
		int size = returns.entrySet().iterator().next().getValue().size();
		double [][] data = new double[size][returns.size()];

		for(Map.Entry<String, List<Float>> entry : returns.entrySet()) {
			int column = mappings.getKey(entry.getKey());
			int row = 0;
			for(Float f : entry.getValue()) {
				data[row++][column] = f.doubleValue();
			}
		}
		return data;
	}

	private static Map<String, Float> findExpectedReturns(
			Map<String, List<Float>> allReturns) {
		Map<String, Float> expectedReturns = new HashMap<String, Float>();
		for(Map.Entry<String, List<Float>> entry : allReturns.entrySet()) {
			float running = 0;
			for(Float f : entry.getValue()) {
				running += f.floatValue();
			}
			float dailyAverage = running / entry.getValue().size();
			float annualized = 250 * dailyAverage;
			expectedReturns.put(entry.getKey(), annualized);
		}
		
		return expectedReturns;
	}

	private static Map<String, List<Float>> findHistorialReturns(Map<String, Stock> stocks, Calendar start, Calendar end, Interval interval)
			throws IOException {
		
		final Map<String, List<Float>> allReturns = new HashMap<String, List<Float>>();
		for(Stock stock : stocks.values()) {
			List<HistoricalQuote> quotes = stock.getHistory(start, end, interval);
			
			List<Float> returns = new ArrayList<Float>(quotes.size() - 1);
			float last = -1f;
			for(HistoricalQuote quote : quotes) {
				if(last == -1f) {
					last = quote.getAdjClose().floatValue();
				} else {
					float curr = quote.getAdjClose().floatValue();
					float change = 100f * (last - curr) / curr;
					
					returns.add(Float.valueOf(change));
					
					last = curr;
				}
			}
			
			allReturns.put(stock.getSymbol(), returns);
		}
		return allReturns;
	}
}
