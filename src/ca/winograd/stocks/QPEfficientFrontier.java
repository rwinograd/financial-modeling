package ca.winograd.stocks;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import com.joptimizer.functions.ConvexMultivariateRealFunction;
import com.joptimizer.functions.LinearMultivariateRealFunction;
import com.joptimizer.functions.PDQuadraticMultivariateRealFunction;
import com.joptimizer.optimizers.JOptimizer;
import com.joptimizer.optimizers.OptimizationRequest;

public class QPEfficientFrontier implements EfficientFrontier {

	private static final double TOLERANCE = 1E-6;
	
	public static QPEfficientFrontier newInstance(BiMap mappings, RealMatrix returns, RealMatrix covariance) {
		// Math taken from: http://faculty.washington.edu/ezivot/econ424/portfoliotheorynoshortsalesslides.pdf
		int size = returns.getRowDimension();
		Preconditions.checkArgument(size == mappings.size(), "Mappings did not have correct size");
		Preconditions.checkArgument(size == covariance.getRowDimension(), "Covariance did not have correct number of rows");
		Preconditions.checkArgument(size == covariance.getColumnDimension(), "Covariance did not have correct number of columns");
		
		// Objective function
		double[][] P = covariance.scalarMultiply(2).getData();
		double[] Q = null;
		double r = 0;

		//equalities
		RealMatrix mA = MatrixUtils.createRealMatrix(2, size);
		mA.setRowMatrix(0, returns.transpose());
		mA.setRowMatrix(1, MatrixUtils.createRealMatrix(1, size).scalarAdd(1.0));
		
		// b is to be determined once we know the target u

		//inequalities
		ConvexMultivariateRealFunction[] inequalities = new ConvexMultivariateRealFunction[size];
		for(int i = 0; i < size; i++) {
			inequalities[i] = new LinearMultivariateRealFunction(arrayWithNegativeOneAtPosition(i, size), 0);
		}
		
		return new QPEfficientFrontier(mappings, new PDQuadraticMultivariateRealFunction(P, Q, r), mA.getData(), inequalities, covariance);
	}
	
	private static double[] arrayWithNegativeOneAtPosition(int i, int size) {
		double [] array = new double[size];
		array[i] = -1;
		
		return array;
	}

	private final BiMap mappings;
	private final PDQuadraticMultivariateRealFunction objectiveFunction;
	private final double [][] A;
	private final ConvexMultivariateRealFunction[] inequalities;
	private final RealMatrix covariance;
	
	private QPEfficientFrontier(BiMap mappings, PDQuadraticMultivariateRealFunction objectiveFunction, double [][] A, 
			ConvexMultivariateRealFunction[] inequalities, RealMatrix covariance) {
		this.mappings = mappings;
		this.objectiveFunction = objectiveFunction;
		this.A = A;
		this.inequalities = inequalities;
		this.covariance = covariance;
	}
	
	@Override
	public double getRisk(double u) {
		double [] b = new double[] {u, 1};
		
		//optimization problem
		OptimizationRequest or = new OptimizationRequest();
		or.setF0(objectiveFunction);
		or.setFi(inequalities); //if you want x>0 and y>0
		or.setA(A);
		or.setB(b);
		or.setToleranceFeas(TOLERANCE);
		or.setTolerance(TOLERANCE);
		
		//optimization
		try {
			JOptimizer opt = new JOptimizer();
			opt.setOptimizationRequest(or);
			int returnCode = opt.optimize();
			
			double expectedRisk = 0;
			double [] sol = opt.getOptimizationResponse().getSolution();
			for(int i = 0; i < sol.length; i++) {
				expectedRisk += (sol[i]*covariance.getEntry(i, i)*sol[i]); 
				for(int j = 0; j < sol.length; j++) {
					expectedRisk += 2*sol[i]*sol[j]*covariance.getEntry(i, j);
				}
			}
			
			return Math.sqrt(expectedRisk);
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			// For Now....
			return -1;
		}
	}

	@Override
	public Portfolio getGlobalMinimumPortfolio() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Portfolio getTangencyPortfolio() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Portfolio getPortfolio(double u) {
		double [] b = new double[] {u, 1};
		
		//optimization problem
		OptimizationRequest or = new OptimizationRequest();
		or.setF0(objectiveFunction);
		or.setFi(inequalities); //if you want x>0 and y>0
		or.setA(A);
		or.setB(b);
		or.setToleranceFeas(TOLERANCE);
		or.setTolerance(TOLERANCE);
		
		//optimization
		try {
			JOptimizer opt = new JOptimizer();
			opt.setOptimizationRequest(or);
			int returnCode = opt.optimize();
			
			double expectedRisk = 0;
			double [] sol = opt.getOptimizationResponse().getSolution();
			return new RealMatrixPortfolio(mappings, MatrixUtils.createColumnRealMatrix(sol));
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

}
