package ca.winograd.stocks;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

class LagrangeEfficientFrontier implements EfficientFrontier {

	private final BiMap mKeyMappings;
	
	private final double mA;
	private final double mB;
	private final double mC;
	private final double mDelta;
	
	private final RealMatrixPortfolio mTangencyPortfolio;
	private final RealMatrixPortfolio mGlobalMinimumPortfolio;
		
	public LagrangeEfficientFrontier(BiMap mappings, RealMatrix A, RealMatrix B, RealMatrix C, RealMatrix D, RealMatrix tangencyPortfolio, RealMatrix globalMinimumPortfolio) {
		double delta = D.getEntry(0, 0);
		
		mA = A.getEntry(0,0) / delta;
		mB = B.getEntry(0,0) / delta;
		mC = C.getEntry(0,0) / delta;
		mDelta = delta;
		
		mKeyMappings = mappings.copy();
		mTangencyPortfolio = new RealMatrixPortfolio(mappings, tangencyPortfolio);
		mGlobalMinimumPortfolio = new RealMatrixPortfolio(mappings, globalMinimumPortfolio);
	}

	@Override
	public double getRisk(double u) {
		return Math.sqrt(mA*u*u - 2*mB*u +mC);
	}

	@Override
	public Portfolio getGlobalMinimumPortfolio() {
		return mGlobalMinimumPortfolio;
	}

	@Override
	public Portfolio getTangencyPortfolio() {
		return mTangencyPortfolio;
	}
	
	@Override
	public Portfolio getPortfolio(double u) {
		RealMatrix weightA = mGlobalMinimumPortfolio.getRealMatrix().scalarMultiply(mA * (mC - u*mB) * mDelta);
		RealMatrix weightB = mTangencyPortfolio.getRealMatrix().scalarMultiply(mB * (u*mA - mB) * mDelta);
		
		return new RealMatrixPortfolio(mKeyMappings, weightA.add(weightB));
	}

	@Override
	public String toString() {
		return String.format("%.3f*u^2 - %.3f*u + %.3f", mA, 2*mB, mC);
	}

	public static LagrangeEfficientFrontier newInstance(BiMap keyMappings, RealMatrix expectedReturnsMatrix, RealMatrix covarianceMatrix) {
		final RealMatrix unityVector = unityVector(expectedReturnsMatrix.getRowDimension()); 
		final RealMatrix inverseCovarianceMatrix = MatrixUtils.inverse(covarianceMatrix);
		
		final RealMatrix A = unityVector.transpose().multiply(inverseCovarianceMatrix).multiply(unityVector);
		final RealMatrix B = unityVector.transpose().multiply(inverseCovarianceMatrix).multiply(expectedReturnsMatrix);
		final RealMatrix C = expectedReturnsMatrix.transpose().multiply(inverseCovarianceMatrix).multiply(expectedReturnsMatrix);
		final RealMatrix DELTA = A.multiply(C).subtract(B.multiply(B));

		final RealMatrix tangencyPortfolio =
				inverseCovarianceMatrix.multiply(expectedReturnsMatrix).multiply(MatrixUtils.inverse(B));

		final RealMatrix globalMinimumPortfolio =
				inverseCovarianceMatrix.multiply(unityVector).multiply(MatrixUtils.inverse(A));
		
		return new LagrangeEfficientFrontier(keyMappings, A, B, C, DELTA, tangencyPortfolio, globalMinimumPortfolio);
	}

	private static RealMatrix unityVector(int length) {
		RealMatrix unityVector = MatrixUtils.createRealMatrix(length, 1);
		for(int row = 0; row < unityVector.getRowDimension(); row++) {
			unityVector.setEntry(row, 0, 1);
		}
		return unityVector;
	}
}
