package ca.winograd.stocks;

import com.xeiam.xchart.Chart;
import com.xeiam.xchart.QuickChart;
import com.xeiam.xchart.SwingWrapper;

public class XYPlotMapper {

	public interface Function {
		double getValue(double input);
	}
	
	/**
	 * Plot the given function from X to Y
	 * @param startX
	 * @param startY
	 * @param function
	 * @return
	 */
	public void plot(double startX, double endX, double step, Function function) {
		int length = (int) ((endX - startX) / step);
		
		double [] xData = new double[length];
		double [] yData = new double[length];
		for(int i = 0; i < length; i++) {
			
			double input = startX + i*step;
			xData[i] = input;
			yData[i] = function.getValue(input);
		}
		
		Chart chart = QuickChart.getChart("Efficient Frontier", "Return", "Risk", "Lagrange", xData, yData);
		new SwingWrapper(chart).displayChart();
	}
}
