package ca.winograd.stocks;

public interface EfficientFrontier {
	public double getRisk(double u);
	public Portfolio getGlobalMinimumPortfolio();
	public Portfolio getTangencyPortfolio();
	public Portfolio getPortfolio(double u);
}
