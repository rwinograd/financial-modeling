package ca.winograd.stocks;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BiMap {

	private final Map<String, Integer> mMap;
	private final String[] mValues;

	public BiMap() {
		this(new String[]{});
	}
	
	public BiMap(List<String> keys) {
		mValues = keys.toArray(new String[keys.size()]);
		mMap = createLookupTable(keys);
	}

	public BiMap(String [] keys) {
		mValues = keys.clone();
		mMap = createLookupTable(Arrays.asList(keys));
	}

	public BiMap(Map<String, Integer> map, String [] keys) {
		mValues = keys.clone();
		mMap = new HashMap<String, Integer>(map);
	}
	
	private static Map<String, Integer> createLookupTable(Iterable<String> stocks) {
		HashMap<String,Integer> map = new HashMap<String, Integer>();
		
		int i = 0;
		for(String stock : stocks) {
			map.put(stock, Integer.valueOf(i++));
		}
		
		return map;
	}
	
	public int size() {
		return mValues.length;
	}
	
	public int getKey(String string) {
		return mMap.get(string);
	}
	
	public String getValue(int key) {
		return mValues[key];
	}

	public BiMap copy() {
		return new BiMap(mMap, mValues);
	}
}
