package ca.winograd.stocks;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.junit.Test;

public class LagrangeEfficientFrontierTest {

	@Test
	public void sanityCheckMyMath() {
		RealMatrix returns = MatrixUtils.createColumnRealMatrix(new double[] {14,12,15,7});
		RealMatrix covariance = MatrixUtils.createRealMatrix(new double[][]{
			{185,  86.5, 80, 20},
			{86.5, 196,  76, 13.5},
			{80, 76, 411, -19},
			{20, 13.5, -19, 25}
		});
		
		LagrangeEfficientFrontier uut = LagrangeEfficientFrontier.newInstance(new BiMap(), returns, covariance);
		System.out.println(uut.getRisk(10));
	}
}
